import React, { useEffect, useState } from "react";
import api from "../../services/api";

import {
  Container,
  ProductsTable,
  Product,
  Name,
  GraphContainer,
  GraphContent,
  RightContainer,
  StockSize,
  TableHeader,
} from "./styles";

interface ProductProps {
  name: string;
  quantity: number;
  stockLimit: number;
}

interface ProductApiProps {
  name: string;
  qty_stocked: number;
  max_stock_limit: number;
}

const CurrentStockContent: React.FC = () => {
  const [products, setProducts] = useState<ProductProps[]>();

  async function getProducts() {
    try {
      const { data: productsData } = await api.get<ProductApiProps[]>(
        "/products"
      );

      const parsedProducts = productsData.map((product) => {
        return {
          name: product.name,
          quantity: product.qty_stocked,
          stockLimit: product.max_stock_limit,
          percent: (product.qty_stocked * 100 + 1) / product.max_stock_limit,
        };
      });

      const orderedProducts = parsedProducts.sort(function orderByPercentage(a, b) {
        if (a.percent < b.percent) {
          return -1;
        }
        if (a.percent > b.percent) {
          return 1;
        }
        return 0;
      });

      setProducts(orderedProducts);
    } catch (error) {
      console.log("Error on CurrentStockContent.getProducts: \n", error);
    }
  }

  function getPercent(value: number, max: number) {
    return (value / max) * 100;
  }

  useEffect(() => {
    getProducts();
  }, []);

  return (
    <Container>
      <ProductsTable>
        <TableHeader>
          <Name>Nome do Produto</Name>
          <Name>Estado do Estoque</Name>
          <RightContainer>Quantidade em Estoque</RightContainer>
        </TableHeader>
        {
          // TODO Fazer o header e conteúdo só aparecer se tiver dados em products
          // TODO Resolver o fato de carregar a página antes de receber um novo produto
          products &&
            products.map((product, index) => (
              <Product key={product.name}>
                <Name>{product.name}</Name>
                <GraphContainer>
                  <GraphContent
                    percent={getPercent(product.quantity, product.stockLimit)}
                  />
                </GraphContainer>
                <RightContainer>
                  <StockSize>{product.quantity}</StockSize>
                </RightContainer>
              </Product>
            ))
        }
      </ProductsTable>
    </Container>
  );
};
export default CurrentStockContent;
