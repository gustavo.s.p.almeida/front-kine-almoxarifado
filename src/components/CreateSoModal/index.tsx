import React, { FormEvent, useState } from "react";
import api from "../../services/api";
import Input from "../Input";

import MyTableExcel from "../ExcelTable";

import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";

import { InputSearchProps } from "../InputSearch";
import Modal from "../Modal";

import ButtonEdit from "../ButtonEdit";
import ButtonRemoveProduct from "../ButtonRemoveProduct";
import Button from "../Button";

import { InputLine, Label } from "./styles";
import { useEffect } from "react";
import AddMaterialModal from "../AddMaterialModal";

interface SOProps {
  id: string;
  SONumber: string;
  client: string;
  seller: string;
  responsibleTechnician: string;
  costOfSO: string;
  running: true;
  completed: false;
  closed: false;
}

interface ProductProps {
  id: string;
  name: string;
  value: number;
  quantity: number;
  measureUnit?: string;
  costCenter?: string;
}

interface MaterialsProps {
  id: string;
  name: string;
  quantity: number;
  value: number;
  totalCost: number;
}

interface CreateSoModalProps {
  openServiceOrderRegistrationModal: boolean;
  setOpenServiceOrderRegistrationModal: React.Dispatch<
    React.SetStateAction<boolean>
  >;

  openEditModal: boolean;
  setOpenEditModal: React.Dispatch<React.SetStateAction<boolean>>;
}

interface ErrorProps {
  response: {
    data: {
      message: string;
    };
  };
}

const CreateSoModal: React.FC<CreateSoModalProps> = ({
  openServiceOrderRegistrationModal,
  setOpenServiceOrderRegistrationModal,
  openEditModal,
  setOpenEditModal,
}) => {
  const [id, setId] = useState("");

  const [openAddMaterialToAnSoModal, setOpenAddMaterialToAnSoModal] =
    useState(false);

  const [product, setProduct] = useState<InputSearchProps | null>(null);

  const [client, setClient] = useState<InputSearchProps | null>(null);
  const [clients, setClients] = useState<InputSearchProps[]>([]);

  const [seller, setSeller] = useState<InputSearchProps | null>(null);
  const [sellers, setSellers] = useState<InputSearchProps[]>([]);

  const [responsibleTechnician, setResponsibleTechnician] =
    useState<InputSearchProps | null>(null);
  const [responsibleTechnicians, setResponsibleTechnicians] = useState<
    InputSearchProps[]
  >([]);

  const [quantity, setQuantity] = useState(0);

  const [SONumber, setSONumber] = useState<string>("");

  const [materials, setMaterials] = useState<MaterialsProps[]>([]);
  const [materialsInStock, setMaterialsInStock] = useState<InputSearchProps[]>(
    []
  );

  function clearInputs() {
    setSONumber("");
    setClient(null);
    setSeller(null);
    setResponsibleTechnician(null);
    setMaterials([]);
    setProduct(null);
    setQuantity(0);
  }

  async function getInputSearchResults() {
    try {
      const { data } = await api.get<ProductProps[]>("/products");

      let parsedData: InputSearchProps[] = [];

      data.forEach((product) => {
        parsedData.push({ id: product.id, name: product.name });
      });
      setMaterialsInStock(parsedData);

      const { data: clients } = await api.get("/clients");
      setClients(clients);

      const { data: sellers } = await api.get("/sellers");
      setSellers(sellers);

      const { data: responsibleTechnicians } = await api.get("/technicians");
      setResponsibleTechnicians(responsibleTechnicians);
    } catch (error) {
      console.log(error);
    }
  }

  async function getSO() {
    try {
      const { data } = await api.get("/serviceOrders");

      const running: SOProps[] = [];

      const completed: SOProps[] = [];

      const closed: SOProps[] = [];

      data.forEach((so: any) => {
        const soParsed = {
          id: so.id,
          SONumber: so.number,
          client: so.client.name,
          seller: so.seller.name,
          responsibleTechnician: so.technician.name,
          costOfSO: so.total_cost || "00,00",
          running: so.running,
          completed: so.completed,
          closed: so.closed,
        };
        if (so.running) {
          running.push(soParsed);
        }
        if (so.completed) {
          completed.push(soParsed);
        }
        if (so.closed) {
          closed.push(soParsed);
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  async function handleServiceOrderRegistration(e: FormEvent) {
    e.preventDefault();

    try {
      const { data: clients } = await api.post("/clients", {
        name: client?.name || "",
      });
      const { data: sellers } = await api.post("/sellers", {
        name: seller?.name || "",
      });
      const { data: technicians } = await api.post("/technicians", {
        name: responsibleTechnician?.name || "",
      });
      let materialsTotalCost = 0;
      materials.forEach((material) => {
        materialsTotalCost += material.totalCost;
      });

      const so = {
        number: SONumber,
        running: true,
        completed: false,
        closed: false,
        man_power_cost: null,
        displacement_cost: null,
        materials: materials.map((data) => ({
          name: data.name,
          qty: data.quantity,
          unit_cost: data.value,
          total_cost: data.totalCost,
        })),
        clientId: clients.id,
        sellerId: sellers.id,
        technicianId: technicians.id,
        materials_total_cost: materialsTotalCost,
      };

      await api.post("/serviceOrders", so);

      setOpenServiceOrderRegistrationModal(false);
      getSO();
      getInputSearchResults();
      clearInputs();
      // TODO Isso aqui é cagada, só pra lidar temporariamente com o problema de precisar atualizar a página do ordens de serviço toda vez que adiciona uma nova
      setTimeout(function () {
        alert("Cadastro de OS feita com sucesso");
        window.location.reload();
      }, 2000);
    } catch (error) {
      const parsedError = error as ErrorProps;
      alert(parsedError?.response?.data?.message);
    }
  }

  function OpenEditModal(material: MaterialsProps) {
    setOpenEditModal(true);
    setId(material.id);
    setProduct({ name: material.name });
    setQuantity(material.quantity);
  }

  function removeProduct(material: MaterialsProps) {
    setMaterials((data) => data.filter((item) => item !== material));
  }

  async function editProduct(material: MaterialsProps) {
    setMaterials((data) =>
      data.map((item) => {
        if (item.id === material.id) {
          return {
            id: material.id,
            value: item.value,
            name: material.name,
            quantity: material.quantity,
            totalCost: material.quantity * item.value,
          };
        }
        return item;
      })
    );
  }

  async function handleEditSubmit(e: FormEvent) {
    e.preventDefault();
    try {
      const { data } = await api.get(`/products?name=${product!.name}`);

      const material: MaterialsProps = {
        id: id,
        name: product?.name || "",
        value: data[0].value,
        quantity,
        totalCost: data[0].value * quantity,
      };
      editProduct(material);
      setOpenEditModal(false);
    } catch (error) {
      const parsedError = error as ErrorProps;
      console.log(parsedError?.response?.data?.message);
    }
  }

  useEffect(() => {
    getInputSearchResults();
  }, []);

  useEffect(() => {
    if (openServiceOrderRegistrationModal) {
      clearInputs();
    }
  }, [openServiceOrderRegistrationModal]);

  return (
    <>
      {openServiceOrderRegistrationModal && (
        <Modal
          title="Cadastro de Ordem de Serviço"
          handleSubmit={handleServiceOrderRegistration}
          setOpenModal={setOpenServiceOrderRegistrationModal}
          style={{
            opacity: openAddMaterialToAnSoModal || openEditModal ? 0 : 1,
          }}
        >
          <div>
            <Input
              label="Número da OS"
              placeholder="Insira o número da OS"
              value={SONumber}
              type="text"
              min="0"
              onChange={(e) => {
                setSONumber(e.target.value);
              }}
            />
            <Input
              inputSearch
              data={clients}
              label="Cliente"
              placeholder="Insira o cliente"
              inputSearchValue={client}
              setValue={setClient}
            />
            <Input
              inputSearch
              data={sellers}
              label="Vendedor"
              placeholder="Insira o nome do vendedor"
              inputSearchValue={seller}
              setValue={setSeller}
            />
            <Input
              inputSearch
              data={responsibleTechnicians}
              label="Técnico Responsável"
              placeholder="Insira o nome do técnico responsável"
              inputSearchValue={responsibleTechnician}
              setValue={setResponsibleTechnician}
            />

            <InputLine
              style={{
                gridTemplateColumns: "1fr 2.5fr",
                padding: 0,
                gap: 60,
              }}
            >
              <Label>Materiais</Label>
              <div>
                <Label className="label-top">
                  {materials.length !== 0 ? (
                    <MyTableExcel
                      style={{ margin: 0 }}
                      columns={[
                        { name: "Material" },
                        { name: "Quantidade" },
                        { name: "Valor Total" },
                        { name: "Ações" },
                      ]}
                    >
                      {materials.map((material: MaterialsProps) => (
                        <TableRow key={material.name}>
                          <TableCell width={150} component="th" scope="row">
                            {material.name}
                          </TableCell>
                          <TableCell align="left">
                            {material.quantity}
                          </TableCell>
                          <TableCell align="left">
                            R$ {material.totalCost}
                          </TableCell>
                          <TableCell align="right">
                            <ButtonEdit
                              onClick={() => OpenEditModal(material)}
                            />
                            <ButtonRemoveProduct
                              onClick={() => removeProduct(material)}
                            />
                          </TableCell>
                        </TableRow>
                      ))}
                    </MyTableExcel>
                  ) : (
                    "Sem itens alocados a essa OS"
                  )}
                </Label>
              </div>
            </InputLine>
          </div>
          <>
            <Button
              type="button"
              outline
              color="#6558F5"
              onClick={() => {
                setOpenServiceOrderRegistrationModal(false);
              }}
            >
              Cancelar
            </Button>
            <Button type="submit" color="#1AAE9F">
              Criar OS
            </Button>
          </>
        </Modal>
      )}
      {/* FIXME: Esse add material não funciona (provavelmente pq não passa o ID da OS, investigar)*/}
      <AddMaterialModal
        openAddMaterialModal={openAddMaterialToAnSoModal}
        setOpenAddMaterialModal={setOpenAddMaterialToAnSoModal}
        isEditableModal={openEditModal}
        setIsEditableModal={setOpenEditModal}
        SONumber={SONumber}
        setSONumber={setSONumber}
        client={client}
        setClient={setClient}
        materials={materials}
        setMaterials={setMaterials}
        noClearInputsOnCloseModal
      />

      {openEditModal && (
        <Modal
          title="Editar Produto"
          handleSubmit={handleEditSubmit}
          setOpenModal={setOpenEditModal}
          style={{ zIndex: 100 }}
        >
          <div>
            <Input
              inputSearch
              data={materialsInStock}
              label="Nome do Produto"
              noAddOption
              placeholder="Insira o nome do Produto"
              inputSearchValue={product}
              setValue={setProduct}
            />
            <Input
              label="Quantidade"
              placeholder="Insira a Quantidade Comprada"
              type="text"
              min="0"
              value={quantity === 0 ? "" : quantity}
              onChange={(e) => {
                setQuantity(Number(e.target.value));
              }}
            />
          </div>
          <>
            <Button
              type="button"
              outline
              color="#6558F5"
              onClick={() => {
                setOpenEditModal(false);
              }}
            >
              Cancelar
            </Button>
            <Button type="submit" style={{ width: 160 }} color="#1AAE9F">
              Salvar alterações
            </Button>
          </>
        </Modal>
      )}
    </>
  );
};

export default CreateSoModal;
