import React, { InputHTMLAttributes, forwardRef } from "react";

import { InputContainer, Label, InputStyled } from "./styles";

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  label: string;
}

const InputWithLabelAtTheTop: React.ForwardRefRenderFunction<
  HTMLInputElement,
  InputProps
> = ({ label, ...rest }, ref) => {
  return (
    <InputContainer>
      <Label>{label}</Label>
      <InputStyled ref={ref} required {...rest} />
    </InputContainer>
  );
};

export default forwardRef(InputWithLabelAtTheTop);
