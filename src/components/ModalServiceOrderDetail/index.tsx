import React from "react";

import {
  Wrapper,
  Container,
  Header,
  Title,
  CloseIcon,
  Content,
  ButtonsContainer,
} from "./styles";

interface ModalProps {
  title: string;
  children: React.ReactNode[];
  setOpenModal: React.Dispatch<React.SetStateAction<boolean>>;
  style?: React.CSSProperties;
}

const ModalServiceOrderDetail = ({
  title,
  children,
  setOpenModal,
  style,
}: ModalProps) => {
  return (
    <Wrapper style={style}>
      <Container>
        <Header>
          <Title>{title}</Title>
          <CloseIcon
            onClick={() => {
              setOpenModal(false);
            }}
          />
        </Header>
        <Content>
          {children[0]}
          <ButtonsContainer>{children[1]}</ButtonsContainer>
        </Content>
      </Container>
    </Wrapper>
  );
};

export default ModalServiceOrderDetail;
