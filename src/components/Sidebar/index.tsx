import React, { useContext, useEffect, useState } from "react";
// TODO REFATORA TODO ESSE COMPONENTE
// - Ele força a repetição de código por conta desse role de active page (não faz sentido sapoha)
// - Ao adicionar novos itens na sidenav ela quebra (fica zuado o indicador da página atual)
// - Já tinha diversos outros TODOS ignorados pelo Heitor
import AvatarImage from "../../assets/user.png";

import { MdEdit } from "react-icons/md";

import {
  Container,
  CloseSidebar,
  CloseIcon,
  Content,
  AvatarContainer,
  Avatar,
  EditAvatarButton,
  Name,
  Menu,
  Indicator,
  MenuItem,
  MenuItemInvisivel,
} from "./styles";
import api from "../../services/api";
import { SideBarContext } from "../../contexts/SideBarContext";

import { MdPlaylistAdd, MdAddShoppingCart } from "react-icons/md";

import { FiShoppingBag } from "react-icons/fi";
import { RiFileEditFill, RiLoginBoxFill } from "react-icons/ri";
import { AiFillHome, AiFillSetting } from "react-icons/ai";
import CreateSoModal from "../CreateSoModal";
import { AuthContext } from "../../contexts/AuthContext";

const Sidebar: React.FC = () => {
  const { activePage, setActivePage } = useContext(SideBarContext);
  const { isAuthenticated, handleLogoff } = useContext(AuthContext);

  const [
    openServiceOrderRegistrationModal,
    setOpenServiceOrderRegistrationModal,
  ] = useState(false);

  const [openEditModal, setOpenEditModal] = useState(false);

  const [userName, setUserName] = useState("");
  const [avatar, setAvatar] = useState<File | string>("");
  const [avatarUrl, setAvatarUrl] = useState(AvatarImage);

  const { open, setOpen } = useContext(SideBarContext);

  // TODO Verificar quais dessas funções são realmente necessárias e quais podem ser importadas (Elas não podem fazer parte dos componentes?)

  const handleAvatarChange = async (files: FileList | null) => {
    try {
      if (files) {
        setAvatar(files[0]);
        const dataToBackend = new FormData();
        dataToBackend.append("avatar", avatar);
        if (localStorage.getItem("accessLevel") === "admin") {
          const { data } = await api.patch("/admins/avatar", dataToBackend);
          setAvatarUrl(
            `https://kine-almoxarifado.herokuapp.com/files/${data.avatar}`
          );
        } else {
          const { data } = await api.patch("/users/avatar", dataToBackend);
          setAvatarUrl(
            `https://kine-almoxarifado.herokuapp.com/files/${data.avatar}`
          );
        }
      }
    } catch (error) {
      alert("Falha ao alterar avatar");
      console.log(error);
    }
  };

  async function getProfile() {
    try {
      let profile = { avatar: "", name: "" };
      const { data } = await api.get(`/users/profile`);
      profile.avatar = data.avatar;
      profile.name = data.name;
      if (!profile.name) {
        const { data } = await api.get(`/admins/profile`);
        profile.avatar = data.avatar;
        profile.name = data.name;
      }
      setAvatarUrl(`${window.location.origin}/files/${profile.avatar}`);
      setUserName(profile.name);
    } catch (error) {
      console.log("Erro na Sidebar->index.getProfile()", error);
    }
  }

  useEffect(() => {
    getProfile();
  }, [isAuthenticated]);

  // TODO Pq as modais não são globais dentro do software pra vc só chamar elas com padrametros de qualquer lugar do código? (analisar viabilidade mais a fundo)
  return (
    <>
      <Container open={open}>
        {setOpen && (
          <CloseSidebar
            onClick={() => {
              setOpen(false);
            }}
          >
            Fechar Menu <CloseIcon />
          </CloseSidebar>
        )}
        <Content>
          <AvatarContainer>
            <Avatar src={avatarUrl} />
            <EditAvatarButton htmlFor="upload">
              <MdEdit color={"#6558F5"} fontSize={20} />
            </EditAvatarButton>
            <input
              id="upload"
              type="file"
              onChange={(e) => handleAvatarChange(e.target.files)}
              style={{ opacity: 0 }}
            />
          </AvatarContainer>
          <Name>{userName}</Name>
          <Menu>
            <Indicator index={activePage} />
            <MenuItem
              to="/"
              $active={activePage === 0}
              onClick={() => setActivePage(0)}
            >
              <AiFillHome className="icon" />
              Início
            </MenuItem>
            <MenuItem
              to="/estoque-atual"
              $active={activePage === 1}
              onClick={() => setActivePage(1)}
            >
              <FiShoppingBag className="icon" />
              Estoque Atual
            </MenuItem>
            <MenuItem
              to="/estoque-atual"
              $active={activePage === 2}
              onClick={() => setActivePage(2)}
            >
              <MdAddShoppingCart className="icon" />
              Nova Compra
            </MenuItem>
            {/* TODO Adicionar saida do estoque*/}
            {/* TODO Adicionar retorno ao estoque*/}
            <MenuItem
              to="/gestao-de-os"
              $active={activePage === 3}
              onClick={() => setActivePage(3)}
            >
              <RiFileEditFill className="icon" />
              Gestão das OS's
            </MenuItem>
            <MenuItem
              to="#"
              $active={activePage === 4}
              onClick={() => {
                setOpenServiceOrderRegistrationModal(true);
              }}
            >
              <MdPlaylistAdd className="icon" />
              Nova OS
            </MenuItem>
            {localStorage.getItem("accessLevel") === "admin" && (
              <MenuItem
                to="/area-adminstrativa"
                $active={activePage === 5}
                onClick={() => setActivePage(5)}
              >
                <AiFillSetting className="icon" />
                Área administrativa
              </MenuItem>
            )}
            <MenuItem
              to="/entrar"
              $active={activePage === 6}
              onClick={() => handleLogoff()}
            >
              <RiLoginBoxFill className="icon" />
              Sair
            </MenuItem>
            {localStorage.getItem("accessLevel") === "user" && (
              <MenuItemInvisivel
                to="/preenchimento"
                $active={activePage === 7}
                onClick={() => setActivePage(7)}
              >
                <AiFillSetting className="icon" />
                Esse item é invisivel
                {/* Esse item só existe pq o heitor fez esse componente todo cagado e ai teria que fazer uma alteraçao condicional nos estilos com base em uma info do app (inviável, melhor refatorar ele dps) */}
              </MenuItemInvisivel>
            )}
          </Menu>
        </Content>
        <CreateSoModal
          openServiceOrderRegistrationModal={openServiceOrderRegistrationModal}
          setOpenServiceOrderRegistrationModal={
            setOpenServiceOrderRegistrationModal
          }
          openEditModal={openEditModal}
          setOpenEditModal={setOpenEditModal}
        />
      </Container>
    </>
  );
};

export default Sidebar;
