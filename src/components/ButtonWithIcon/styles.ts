import styled from "styled-components";

import Tolltip from "../Tooltip";

export const Container = styled(Tolltip)`
  width: 30px;
  height: 30px;

  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 5px;

  display: inline-flex;

  margin-left: 10px;

  background-color: #207868;

  cursor: pointer;

  & ~ #overridePosition {
    font-size: 15px;
    padding: 6px 16px;
  }
`;
