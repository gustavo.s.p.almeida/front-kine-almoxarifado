import React from "react";
import { IconType } from "react-icons/lib";

import { Container } from "./styles";

interface ButtonWithIconProps {
  Icon: IconType;
  onClick?: () => void;
  style?: React.CSSProperties;
  tooltipText: string;
  tooltipTextBackgroundColor?: string;
}

const ButtonWithIcon: React.FC<ButtonWithIconProps> = ({
  Icon,
  onClick,
  style,
  tooltipText,
  tooltipTextBackgroundColor = "#207868",
}) => {
  return (
    <Container
      tooltipText={tooltipText}
      tooltipTextBackgroundColor={tooltipTextBackgroundColor}
      onClick={onClick}
      style={style}
    >
      <Icon color="#fff" fontSize={17} />
    </Container>
  );
};

export default ButtonWithIcon;
