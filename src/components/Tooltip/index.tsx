import ReactTooltip from "react-tooltip";

interface TooltipProps extends React.HTMLAttributes<HTMLDivElement> {
  tooltipText: string;
  tooltipTextBackgroundColor: string;
}

const Tooltip: React.FC<TooltipProps> = ({
  children,
  style,
  onClick,
  className,
  tooltipText,
  tooltipTextBackgroundColor,
}) => {
  return (
    <>
      <div
        style={style}
        onClick={onClick}
        data-tip={tooltipText}
        data-for="overridePosition"
        className={className}
      >
        {children}
      </div>
      <ReactTooltip
        id="overridePosition"
        place="top"
        backgroundColor={tooltipTextBackgroundColor}
        effect="solid"
      />
    </>
  );
};

export default Tooltip;
