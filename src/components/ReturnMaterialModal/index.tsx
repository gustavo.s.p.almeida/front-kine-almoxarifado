import React, { FormEvent, useEffect, useState } from "react";
import api from "../../services/api";
import Button from "../Button";
import Input from "../Input";
import { InputSearchProps } from "../InputSearch";
import Modal from "../Modal";

interface ReturnMaterialModalProps {
  serviceOrderId?: string;
  openReturnAMaterialFromAnOsModal: boolean;
  setOpenReturnAMaterialFromAnOsModal: React.Dispatch<
    React.SetStateAction<boolean>
  >;

  SONumber: string;
  setSONumber: React.Dispatch<React.SetStateAction<string>>;

  client: InputSearchProps | null;
  setClient: React.Dispatch<React.SetStateAction<InputSearchProps | null>>;

  materialsInSO: InputSearchProps[];
  setMaterialsInSO: React.Dispatch<React.SetStateAction<InputSearchProps[]>>;

  modalReturnMaterialEditable?: boolean;
  setModalReturnMaterialEditable?: React.Dispatch<
    React.SetStateAction<boolean>
  >;
}

interface ErrorProps {
  response: {
    data: {
      message: string;
    };
  };
}

interface ProductApiReturnProps {
  id: string;
  name: string;
  qty: number;
}

const ReturnMaterialModal: React.FC<ReturnMaterialModalProps> = ({
  serviceOrderId,
  openReturnAMaterialFromAnOsModal,
  setOpenReturnAMaterialFromAnOsModal,
  SONumber,
  setSONumber,
  client,
  setClient,
  materialsInSO,
  setMaterialsInSO,
  modalReturnMaterialEditable,
  setModalReturnMaterialEditable,
}) => {
  const [id, setId] = useState(serviceOrderId);
  const [closeModalAfterSave, setCloseModalAfterSave] = useState(false); // setAddMaterialAndClose
  const [productInputValue, setProductInputValue] =
    useState<InputSearchProps | null>(null);
  const [quantityInputValue, setQuantityInputValue] = useState(0);

  async function getClient() {
    try {
      if (SONumber) {
        const { data } = await api.get(`/serviceOrders?number=${SONumber}`);
        setId(data.id);
        setClient({ name: data ? data.client.name : "Sem cliente" });
        let parsedData: InputSearchProps[] = [];

        data.materials.forEach((product: ProductApiReturnProps) => {
          if (product.qty > 0) {
            parsedData.push({ id: product.id, name: product.name });
          }
        });

        setMaterialsInSO(parsedData);
      }
    } catch (error) {
      console.log(error);
    }
  }

  async function handleReturnServiceOrder(e: FormEvent) {
    e.preventDefault();

    try {
      await api.post(`/serviceOrders/returnMaterial/${id}`, {
        product_name: productInputValue?.name,
        qty: quantityInputValue,
      });
      if (closeModalAfterSave) {
        setOpenReturnAMaterialFromAnOsModal(false);
        alert("Material Retornou ao estoque");
        return true;
      }
      setProductInputValue(null);
      setQuantityInputValue(0);
      setMaterialsInSO([]);
      getClient();
      alert("Material Retornou ao estoque");
    } catch (error) {
      console.log(error);
      const parsedError = error as ErrorProps;
      alert(parsedError?.response?.data?.message);
    }
  }

  useEffect(() => {
    setId(serviceOrderId);
  }, [serviceOrderId]);

  useEffect(() => {
    if (!openReturnAMaterialFromAnOsModal) {
      setSONumber("");
      setClient(null);
      setProductInputValue(null);
      setQuantityInputValue(0);
    }
  }, [
    openReturnAMaterialFromAnOsModal,
    setSONumber,
    setClient,
    setProductInputValue,
    setQuantityInputValue,
  ]);

  return (
    <>
      {openReturnAMaterialFromAnOsModal && (
        <Modal
          title="Retornar material ao estoque"
          handleSubmit={handleReturnServiceOrder}
          setOpenModal={setOpenReturnAMaterialFromAnOsModal}
        >
          <div>
            <Input
              label="Número da OS"
              disabled={!modalReturnMaterialEditable}
              onChange={
                modalReturnMaterialEditable
                  ? (e) => {
                      setSONumber(e.target.value);
                    }
                  : undefined
              }
              value={SONumber}
              onBlur={getClient}
            />
            <Input label="Cliente" disabled={true} value={client?.name} />
            <Input
              inputSearch
              noAddOption
              data={materialsInSO}
              label="Produto"
              placeholder="Ex: Mangueira, Parafuso, Suporte, etc..."
              inputSearchValue={productInputValue}
              setValue={setProductInputValue}
            />
            <Input
              label="Quantidade"
              placeholder="Informe a quantidade do material"
              type="text"
              value={quantityInputValue === 0 ? "" : quantityInputValue}
              min="0"
              onChange={(e) => {
                setQuantityInputValue(Number(e.target.value));
              }}
            />
          </div>
          <>
            <Button
              type="button"
              outline
              color="#6558F5"
              onClick={() => {
                setOpenReturnAMaterialFromAnOsModal(false);
              }}
            >
              Cancelar
            </Button>
            <div style={{ display: "flex" }}>
              <Button
                outline
                type="submit"
                color="#1AAE9F"
                onClick={() => {
                  setCloseModalAfterSave(false);
                }}
              >
                Salvar e retornar mais itens
              </Button>
              <Button
                style={{ marginLeft: 20 }}
                type="submit"
                color="#1AAE9F"
                onClick={() => {
                  setCloseModalAfterSave(true);
                }}
              >
                Salvar
              </Button>
            </div>
          </>
        </Modal>
      )}
    </>
  );
};

export default ReturnMaterialModal;
