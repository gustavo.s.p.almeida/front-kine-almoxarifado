import React, { FormEvent, useState } from "react";
import api from "../../services/api";
import Input from "../Input";

import { InputSearchProps } from "../InputSearch";
import Modal from "../Modal";

import Button from "../Button";

import randomId from "../../utils/randomId";
import { useEffect } from "react";

interface ProductProps {
  id: string;
  name: string;
  value: number;
  quantity: number;
  measureUnit?: string;
  costCenter?: string;
}

interface MaterialsProps {
  id: string;
  name: string;
  quantity: number;
  value: number;
  totalCost: number;
}

interface ErrorProps {
  response: {
    data: {
      message: string;
    };
  };
}

interface AddMaterialModalProps {
  prefilledServiceOrderId?: string;
  openAddMaterialModal: boolean;
  setOpenAddMaterialModal: React.Dispatch<React.SetStateAction<boolean>>;
  isEditableModal?: boolean;
  setIsEditableModal?: React.Dispatch<React.SetStateAction<boolean>>;
  SONumber: string;
  setSONumber: React.Dispatch<React.SetStateAction<string>>;
  client: InputSearchProps | null;
  setClient: React.Dispatch<React.SetStateAction<InputSearchProps | null>>;
  materials?: MaterialsProps[];
  setMaterials?: React.Dispatch<React.SetStateAction<MaterialsProps[]>>;
  onSubmitPost?: boolean;
  noClearInputsOnCloseModal?: boolean;
}

const AddMaterialModal: React.FC<AddMaterialModalProps> = ({
  prefilledServiceOrderId,
  openAddMaterialModal,
  setOpenAddMaterialModal,
  SONumber,
  setSONumber,
  client,
  setClient,
  isEditableModal,
  setIsEditableModal,
  materials,
  setMaterials,
  onSubmitPost,
  noClearInputsOnCloseModal,
}) => {
  const [serviceOrderId, setServiceOrderId] = useState(prefilledServiceOrderId);

  const [closeModalAfterSave, setCloseModalAfterSave] = useState(false); // setAddMaterialAndClose

  const [productInputValue, setProductInputValue] =
    useState<InputSearchProps | null>(null);

  const [quantityInputValue, setQuantityInputValue] = useState(0);

  const [materialsInStock, setMaterialsInStock] = useState<InputSearchProps[]>(
    []
  );

  async function getInputSearchResults() {
    try {
      const { data } = await api.get<ProductProps[]>("/products");

      let parsedData: InputSearchProps[] = [];

      data.forEach((productInputValue) => {
        parsedData.push({
          id: productInputValue.id,
          name: productInputValue.name,
        });
      });
      setMaterialsInStock(parsedData);
    } catch (error) {
      console.log(error);
    }
  }

  async function handleAddMaterialModalSubmit(e: FormEvent) {
    e.preventDefault();
    // TODO Esse onSubmitPost é realmente necessário? Tá bem estranho isso, provavelmente é gansada
    if (onSubmitPost) {
      try {
        await api.post(`addMaterial/${serviceOrderId}`, {
          product_id: productInputValue!.id,
          qty: quantityInputValue,
        });
        if (closeModalAfterSave) {
          setOpenAddMaterialModal(false);
          alert("Material alocado");
          return true;
        }
        setOpenAddMaterialModal(true);
        setProductInputValue(null);
        setQuantityInputValue(0);
        alert(`Material alocado à OS: ${SONumber}. Insira o próximo item.`);
      } catch (error) {
        const parsedError = error as ErrorProps;
        console.log(`Erro ao adicionar material à os...\n 
         product.id: ${productInputValue?.id}\n 
         qty: ${quantityInputValue}\n 
         error: ${parsedError?.response?.data?.message}`);

        alert(
          `Erro ao adicionar material à ordem de serviço: ${parsedError?.response?.data?.message}`
        );
      }
    } else {
      try {
        const { data } = await api.get(
          `/products?name=${productInputValue!.name}`
        );
        //FIXME: O que tá rolando aqui? Pq randonID? Refactor Importante
        if (closeModalAfterSave) {
          if (setMaterials && materials) {
            const materialAlreadyExists = materials.find(
              (material) => productInputValue!.name === material.name
            );
            if (materialAlreadyExists) {
              setMaterials((oldValue) => {
                return oldValue.map((material) => {
                  if (material.name === materialAlreadyExists.name) {
                    return {
                      ...material,
                      totalCost:
                        material.totalCost + data[0].value * quantityInputValue,
                      quantity: material.quantity + quantityInputValue,
                    };
                  }
                  return material;
                });
              });
            } else {
              setMaterials([
                ...materials,
                {
                  id: randomId(),
                  name: productInputValue!.name,
                  quantity: quantityInputValue,
                  value: data[0].unit_cost,
                  totalCost: data[0].value * quantityInputValue,
                },
              ]);
            }
          }
          setOpenAddMaterialModal(false);
        } else {
          if (setMaterials && materials) {
            setMaterials([
              ...materials,
              {
                id: randomId(),
                name: productInputValue!.name,
                quantity: quantityInputValue,
                value: data[0].unit_cost,
                totalCost: data[0].value * quantityInputValue,
              },
            ]);
          }
          setOpenAddMaterialModal(true);
          setProductInputValue(null);
          setQuantityInputValue(0);
          alert("Material alocado (sem onSubmitPost)");
        }
      } catch (error) {
        console.log(`Erro dentro do else do "onSubmitPost" (??????) ${error}`);
        const parsedError = error as ErrorProps;
        alert(parsedError?.response?.data?.message);
      }
    }
    // TODO ???
    if (setMaterials && materials) {
      setMaterials((data) => {
        return data.map((material) => {
          const totalCost = Number(material.value) * Number(material.quantity);
          return { ...material, totalCost };
        });
      });
    }
    //TODO ????
    if (setIsEditableModal) {
      setIsEditableModal(false);
    }
  }

  async function getServiceOrderDetails() {
    try {
      const { data } = await api.get(`/serviceOrders?number=${SONumber}`);
      setServiceOrderId(data.id);
      setClient({ name: data ? data.client.name : "Sem cliente" });
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getInputSearchResults();
  }, []);

  useEffect(() => {
    setServiceOrderId(prefilledServiceOrderId);
  }, [prefilledServiceOrderId]);

  useEffect(() => {
    if (!openAddMaterialModal && !noClearInputsOnCloseModal) {
      setSONumber("");
      setClient(null);
      setProductInputValue(null);
      setQuantityInputValue(0);
    }
  }, [
    openAddMaterialModal,
    setSONumber,
    setClient,
    setProductInputValue,
    setQuantityInputValue,
    noClearInputsOnCloseModal,
  ]);

  return (
    <>
      {openAddMaterialModal && (
        <Modal
          title="Adicionar Material à uma OS"
          handleSubmit={handleAddMaterialModalSubmit}
          setOpenModal={setOpenAddMaterialModal}
        >
          <div>
            <Input
              label="Número da OS"
              onChange={
                isEditableModal
                  ? (e) => {
                      setSONumber(e.target.value);
                    }
                  : undefined
              }
              disabled={!isEditableModal}
              value={SONumber}
              onBlur={getServiceOrderDetails}
            />
            <Input label="Cliente" disabled={true} value={client?.name || ""} />
            <Input
              inputSearch
              noAddOption
              data={materialsInStock}
              label="Produto"
              placeholder="Ex: Mangueira, Parafuso, Suporte, etc..."
              inputSearchValue={productInputValue}
              setValue={setProductInputValue}
            />

            <Input
              label="Quantidade"
              placeholder="Informe a quantidade do material"
              type="text"
              value={quantityInputValue === 0 ? "" : quantityInputValue}
              min="0"
              onChange={(e) => {
                setQuantityInputValue(Number(e.target.value));
              }}
            />
          </div>
          <>
            <Button
              type="button"
              outline
              color="#6558F5"
              onClick={() => {
                setOpenAddMaterialModal(false);
              }}
            >
              Voltar para OS
            </Button>
            <div style={{ display: "flex" }}>
              <Button
                outline
                type="submit"
                color="#1AAE9F"
                onClick={() => {
                  setCloseModalAfterSave(false);
                }}
              >
                Salvar e Adicionar Mais itens
              </Button>
              <Button
                style={{ marginLeft: 20 }}
                type="submit"
                color="#1AAE9F"
                onClick={() => {
                  setCloseModalAfterSave(true);
                }}
              >
                Adicionar Material
              </Button>
            </div>
          </>
        </Modal>
      )}
    </>
  );
};

export default AddMaterialModal;
