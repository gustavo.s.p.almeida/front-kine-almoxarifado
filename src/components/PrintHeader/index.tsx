import React from "react";
import { Container, Left, Middle, Right } from "./styles";

import HeaderLogo from "../../assets/logoKine.png";

interface PrintHeaderProps {
  pageName: string;
}

const PrintHeader: React.FC<PrintHeaderProps> = ({ pageName }) => {
  return (
    <Container>
      <Left>{pageName}</Left>
      <Middle></Middle>
      <Right src={HeaderLogo} />
    </Container>
  );
};

export default PrintHeader;
