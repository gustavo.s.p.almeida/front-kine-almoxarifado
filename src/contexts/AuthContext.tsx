import React, { useState, useEffect, createContext } from "react";
import api from "../services/api";

interface AuthContextData extends IUser {
  isAuthenticated: boolean;
  setIsAuthenticated: React.Dispatch<React.SetStateAction<boolean>>;
  handleLogin({ email, password }: AuthData): Promise<boolean>;
  handleRegister({ name, email, password, avatar }: IUser): Promise<boolean>;
  handleLogoff(): void;
}

interface AuthData {
  email: string;
  password: string;
}

interface IUser {
  name: string;
  email: string;
  password: string;
  avatar: string | File;
}

interface ErrorProps {
  response: {
    data: {
      message: string;
    };
  };
}

export const AuthContext = createContext({} as AuthContextData);

export default function AuthProvider({ children }: any) {
  const [user, setUser] = useState({} as IUser);
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    (async () => {
      const token = localStorage.getItem("token");

      if (token) {
        try {
          api.defaults.headers.authorization = `Bearer ${token}`;
          let profile = {} as IUser;
          const { data: user } = await api.get("/users/profile");
          profile = user;
          if (!profile.name) {
            const { data: admin } = await api.get("/admins/profile");
            profile = admin;
          }
          setUser(profile);
          setIsAuthenticated(true);
        } catch {
          setUser({} as IUser);
        }
      }
      setLoading(false);
    })();
  }, []);

  async function handleLogin({ email, password }: AuthData) {
    try {
      const { data } = await api.post("login", {
        email,
        password,
      });
      localStorage.setItem("token", data.token);
      api.defaults.headers.authorization = `Bearer ${data.token}`;
      if (data.admin) {
        localStorage.setItem("accessLevel", "admin");
        setUser(data.admin);
        setIsAuthenticated(true);
      }
      if (data.user) {
        localStorage.setItem("accessLevel", "user");
        setUser(data.user);
        setIsAuthenticated(true);
      }

      return true;
    } catch (err) {
      const parsedError = err as ErrorProps;
      alert(parsedError?.response?.data?.message);
      setUser({} as IUser);
      setIsAuthenticated(false);

      return false;
    }
  }
  async function handleRegister({ name, email, password, avatar }: IUser) {
    try {
      const dataToBackend = new FormData();

      dataToBackend.append("name", name);
      dataToBackend.append("email", email);
      dataToBackend.append("password", password);
      dataToBackend.append("avatar", avatar);
      dataToBackend.append("awaitingApproval", "true");

      const { data } = await api.post("/users", dataToBackend);
      localStorage.setItem("token", data.token);
      api.defaults.headers.authorization = `Bearer ${data.token}`;
      // TODO No que a mudança do isAuthenticated implica?
      setIsAuthenticated(true);
      setUser(data.user);
      return true;
    } catch (err) {
      console.log("\nAuthContext.handleRegister: ", err);
      setIsAuthenticated(false);
      setUser({} as IUser);
      const parsedError = err as ErrorProps;
      alert(parsedError?.response?.data?.message);
      return false;
    }
  }

  function handleLogoff() {
    localStorage.removeItem("token");
    localStorage.removeItem("activePage");
    api.defaults.headers.authorization = undefined;
    setUser({} as IUser);
    setIsAuthenticated(false);
    setLoading(false);
  }

  if (loading) {
    return null;
  }
  return (
    <AuthContext.Provider
      value={{
        handleLogin,
        handleRegister,
        handleLogoff,
        isAuthenticated,
        setIsAuthenticated,
        ...user,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
}
