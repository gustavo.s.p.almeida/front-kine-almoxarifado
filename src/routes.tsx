import React, { useContext } from "react";
import {
  BrowserRouter,
  Switch,
  Route,
  Redirect,
  RouteProps,
} from "react-router-dom";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";
import AdministrativeArea from "./pages/AdministrativeArea";
import CurrentStock from "./pages/CurrentStock";
import SoManagement from "./pages/SoManagement";
import Sidebar from "./components/Sidebar";
import Print from "./pages/Print";
import { AuthContext } from "./contexts/AuthContext";

const PrivateRoute = (props: RouteProps) => {
  const { isAuthenticated } = useContext(AuthContext);

  const showSidebar = isAuthenticated && !props.path?.includes("/imprimir");

  return isAuthenticated ? (
    <>
      {showSidebar && <Sidebar />}
      <Route {...props} />
    </>
  ) : (
    <Redirect to="/entrar" />
  );
};

const Routes: React.FC = () => {
  const { isAuthenticated } = useContext(AuthContext);

  return (
    <>
      <BrowserRouter>
        <Switch>
          <PrivateRoute exact path="/" component={Home} />
          <PrivateRoute
            path="/area-adminstrativa"
            component={AdministrativeArea}
          />
          <PrivateRoute exact path="/estoque-atual" component={CurrentStock} />
          <PrivateRoute exact path="/gestao-de-os" component={SoManagement} />
          <PrivateRoute exact path="/imprimir/:id" component={Print} />
          {isAuthenticated ? (
            <Redirect to="/" />
          ) : (
            <Route path="/entrar" component={Login} />
          )}
          {isAuthenticated ? (
            <Redirect to="/" />
          ) : (
            <Route path="/cadastrar" component={Register} />
          )}
          {isAuthenticated ? <Redirect to="/" /> : <Redirect to="/entrar" />}
        </Switch>
      </BrowserRouter>
    </>
  );
};

export default Routes;
