import React, {
  FormEvent,
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from "react";
import MyTableExcel from "../../components/ExcelTable";

import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";

import HeaderPage from "../../components/Header";

import { InputSearchProps } from "../../components/InputSearch";

import {
  Container,
  InputLine,
  Header,
  PlusIcon,
  Section,
  Title,
  ItemsPurchasedContainer,
  TitleProducts,
  Divisor,
  Text,
} from "./styles";
import InputWithLabelAtTheTop from "../../components/InputWithLabelAtTheTop";
import Button from "../../components/Button";
import ButtonWithIcon from "../../components/ButtonWithIcon";

import { ImBoxAdd, ImBoxRemove } from "react-icons/im";
import { IoKey } from "react-icons/io5";
import { FaLock, FaLockOpen } from "react-icons/fa";
import { MdViewHeadline } from "react-icons/md";

import Modal from "../../components/Modal";
import Input from "../../components/Input";
import api from "../../services/api";
import formatDate from "../../utils/formatDate";
import { SideBarContext } from "../../contexts/SideBarContext";
import CreateSoModal from "../../components/CreateSoModal";
import AddMaterialModal from "../../components/AddMaterialModal";
import ReturnMaterialModal from "../../components/ReturnMaterialModal";
import ModalServiceOrderDetail from "../../components/ModalServiceOrderDetail";

interface SOProps {
  id: string;
  SONumber: string;
  client: string;
  seller: string;
  responsibleTechnician: string;
  costOfSO: string;
  running: true;
  completed: false;
  closed: false;
}

interface ProductProps {
  id: string;
  name: string;
  value: number;
  quantity: number;
  measureUnit?: string;
  costCenter?: string;
}

interface ProductApiReturnProps {
  id: string;
  name: string;
  qty: number;
}

interface MaterialsProps {
  id: string;
  name: string;
  quantity: number;
  value: number;
  totalCost: number;
}

interface ErrorProps {
  response: {
    data: {
      message: string;
    };
  };
}
interface FilteredSoProps {
  SORunning: SOProps[];
  SOCompleted: SOProps[];
  SOClosed: SOProps[];
}

const SoManagement = () => {
  const { open } = useContext(SideBarContext);

  const [openEditModal, setOpenEditModal] = useState(false);
  const [serviceOrderId, setServiceOrderId] = useState("");

  const [SORunning, setSORunning] = useState<SOProps[]>([]);
  const [SOCompleted, setSOCompleted] = useState<SOProps[]>([]);
  const [SOClosed, setSOClosed] = useState<SOProps[]>([]);

  const [products, setProducts] = useState<ProductProps[]>([]);

  const [client, setClient] = useState<InputSearchProps | null>(null);

  const [seller, setSeller] = useState<InputSearchProps | null>(null);

  const [responsibleTechnician, setResponsibleTechnician] =
    useState<InputSearchProps | null>(null);

  const [closeAndSeeDetails, setCloseAndSeeDetails] = useState(false);

  const [
    openServiceOrderRegistrationModal,
    setOpenServiceOrderRegistrationModal,
  ] = useState(false);
  const [openAddMaterialToAnSoModal, setOpenAddMaterialToAnSoModal] =
    useState(false);
  const [
    openReturnAMaterialFromAnOsModal,
    setOpenReturnAMaterialFromAnOsModal,
  ] = useState(false);
  const [openEndAnOsModal, setOpenEndAnOsModal] = useState(false);
  const [openOsDetailsModal, setOpenOsDetailsModal] = useState(false);

  const [SONumber, setSONumber] = useState<string>("");
  const [, setProduct] = useState<InputSearchProps | null>(null);
  const [, setQuantity] = useState(0);
  const [manPower, setManPower] = useState(0);
  const [displacement, setDisplacement] = useState(0);
  const [materialsTotalCost, setMaterialsTotalCost] = useState(0);
  const [totalCost, setTotalCost] = useState(0);
  const [openingDate, setOpeningDate] = useState<Date>(new Date());
  const [closingDate, setClosingDate] = useState<Date>(new Date());

  const [, setMaterials] = useState<MaterialsProps[]>([]);

  const [materialsInSO, setMaterialsInSO] = useState<InputSearchProps[]>([]);

  const inputSearchRef = useRef<HTMLInputElement>(null);

  const [filteredSO, setFilteredSO] = useState<FilteredSoProps>({
    SORunning: [],
    SOCompleted: [],
    SOClosed: [],
  });

  const handleChangeSearchInputValue = useCallback(() => {
    const inputSearchCurrent = inputSearchRef.current;

    if (inputSearchCurrent) {
      const SORunningNewValue = SORunning.filter(
        (so) =>
          so.SONumber.toLocaleLowerCase().includes(
            inputSearchCurrent.value.toLocaleLowerCase()
          ) ||
          so.client
            .toLocaleLowerCase()
            .includes(inputSearchCurrent.value.toLocaleLowerCase()) ||
          so.seller
            .toLocaleLowerCase()
            .includes(inputSearchCurrent.value.toLocaleLowerCase()) ||
          so.responsibleTechnician
            .toLocaleLowerCase()
            .includes(inputSearchCurrent.value.toLocaleLowerCase())
      );
      const SOCompletedNewValue = SOCompleted.filter(
        (so) =>
          so.SONumber.toLocaleLowerCase().includes(
            inputSearchCurrent.value.toLocaleLowerCase()
          ) ||
          so.client
            .toLocaleLowerCase()
            .includes(inputSearchCurrent.value.toLocaleLowerCase()) ||
          so.seller
            .toLocaleLowerCase()
            .includes(inputSearchCurrent.value.toLocaleLowerCase()) ||
          so.responsibleTechnician
            .toLocaleLowerCase()
            .includes(inputSearchCurrent.value.toLocaleLowerCase())
      );
      const SOClosedNewValue = SOClosed.filter(
        (so) =>
          so.SONumber.toLocaleLowerCase().includes(
            inputSearchCurrent.value.toLocaleLowerCase()
          ) ||
          so.client
            .toLocaleLowerCase()
            .includes(inputSearchCurrent.value.toLocaleLowerCase()) ||
          so.seller
            .toLocaleLowerCase()
            .includes(inputSearchCurrent.value.toLocaleLowerCase()) ||
          so.responsibleTechnician
            .toLocaleLowerCase()
            .includes(inputSearchCurrent.value.toLocaleLowerCase())
      );

      setFilteredSO({
        SORunning: SORunningNewValue,
        SOCompleted: SOCompletedNewValue,
        SOClosed: SOClosedNewValue,
      });
    }
  }, [SOClosed, SOCompleted, SORunning]);

  function clearInputs() {
    setSONumber("");
    setClient(null);
    setSeller(null);
    setResponsibleTechnician(null);
    setMaterials([]);
    setProduct(null);
    setQuantity(0);
  }

  async function getServiceOrders() {
    try {
      const { data } = await api.get("/serviceOrders");

      const running: SOProps[] = [];

      const completed: SOProps[] = [];

      const closed: SOProps[] = [];

      data.forEach((so: any) => {
        const soParsed = {
          id: so.id,
          SONumber: so.number,
          client: so.client.name,
          seller: so.seller.name,
          responsibleTechnician: so.technician.name,
          costOfSO: so.total_cost || "00,00",
          running: so.running,
          completed: so.completed,
          closed: so.closed,
        };
        if (so.running) {
          running.push(soParsed);
        }
        if (so.completed) {
          completed.push(soParsed);
        }
        if (so.closed) {
          closed.push(soParsed);
        }
      });

      setSORunning(running);
      setSOCompleted(completed);
      setSOClosed(closed);
      setFilteredSO({
        SORunning: running,
        SOCompleted: completed,
        SOClosed: closed,
      });
    } catch (error) {
      console.log(error);
    }
  }

  async function closeOS(closeServiceOrderId: string) {
    try {
      const { data } = await api.patch(
        `/serviceOrders/close/${closeServiceOrderId}`
      );

      setSOCompleted([
        ...SOCompleted,
        {
          id: data.id,
          SONumber: data.number,
          client: data.client.name,
          seller: data.seller.name,
          responsibleTechnician: data.technician.name,
          costOfSO: data.total_cost || "00,00",
          running: data.running,
          completed: data.completed,
          closed: data.closed,
        },
      ]);
      setSORunning((so) => so.filter((el) => el.id !== closeServiceOrderId));
    } catch (error) {
      console.log(error);
    }
  }
  async function openOS(id: string) {
    try {
      const { data } = await api.patch(`/serviceOrders/open/${id}`);

      setSOCompleted((so) => so.filter((el) => el.id !== id));
      setSORunning([
        ...SORunning,
        {
          id: data.id,
          SONumber: data.number,
          client: data.client.name,
          seller: data.seller.name,
          responsibleTechnician: data.technician.name,
          costOfSO: data.total_cost || "00,00",
          running: data.running,
          completed: data.completed,
          closed: data.closed,
        },
      ]);
    } catch (error) {
      console.log(error);
    }
  }

  async function handleEndAnOSSubmit(e: FormEvent) {
    e.preventDefault();
    try {
      const { data } = await api.patch(
        `/serviceOrders/terminate/${serviceOrderId}`,
        {
          man_power_cost: manPower,
          displacement_cost: displacement,
        }
      );

      setOpenEndAnOsModal(false);
      clearInputs();
      getServiceOrders();

      if (closeAndSeeDetails) {
        openOsDetails(data);
        setCloseAndSeeDetails(false);
      }
    } catch (error) {
      console.log(error);
      const parsedError = error as ErrorProps;
      alert(parsedError?.response?.data?.message);
    }
  }

  async function openReturnAMaterialFromAnOs(so: SOProps) {
    setServiceOrderId(so.id);
    setSONumber(so.SONumber);
    setClient({ name: so.client });
    setOpenReturnAMaterialFromAnOsModal(true);
    try {
      const { data: serviceOrder } = await api.get(`/serviceOrders/${so.id}`);

      let parsedMaterials: InputSearchProps[] = [];

      serviceOrder.materials.forEach((product: ProductApiReturnProps) => {
        if (product.qty > 0) {
          parsedMaterials.push({ id: product.id, name: product.name });
        }
      });
      setMaterialsInSO(parsedMaterials);
    } catch (error) {
      console.log(error);
    }
  }

  function openAddAMaterialFromAnOs(so: SOProps) {
    setServiceOrderId(so.id);
    setSONumber(so.SONumber);
    setClient({ name: so.client });
    setOpenAddMaterialToAnSoModal(true);
  }

  function openEndAnOs(so: SOProps) {
    setServiceOrderId(so.id);
    setSONumber(so.SONumber);
    setClient({ name: so.client });
    setDisplacement(0);
    setManPower(0);
    setOpenEndAnOsModal(true);
  }

  async function openOsDetails(so: SOProps) {
    const { data } = await api.get(`/serviceOrders/${so.id}`);
    const materials = data.materials;

    let parsedProducts: ProductProps[] = [];

    materials.forEach((product: any) => {
      parsedProducts.push({
        id: product.id,
        name: product.name,
        value: product.unit_cost,
        quantity: product.qty,
      });
    });
    setServiceOrderId(data.id);
    setProducts(parsedProducts);
    setOpeningDate(data.created_at);
    setClosingDate(data.updated_at);
    setSONumber(data.number);
    setClient({ name: data.client.name });
    setResponsibleTechnician({ name: data.technician.name });
    setSeller({ name: data.seller.name });
    setManPower(data.man_power_cost);
    setDisplacement(data.displacement_cost);
    setTotalCost(data.total_cost);
    setMaterialsTotalCost(data.materials_total_cost);
    setOpenOsDetailsModal(true);
  }

  useEffect(() => {
    getServiceOrders();
  }, [openReturnAMaterialFromAnOsModal, openAddMaterialToAnSoModal]);

  useEffect(() => {
    if (openServiceOrderRegistrationModal) {
      clearInputs();
    }
  }, [openServiceOrderRegistrationModal]);

  useEffect(() => {
    if (inputSearchRef.current?.value) {
      return handleChangeSearchInputValue();
    }

    setFilteredSO({
      SORunning,
      SOCompleted,
      SOClosed,
    });
  }, [SORunning, SOCompleted, SOClosed, handleChangeSearchInputValue]);

  return (
    <>
      <HeaderPage pageName={"Gestão de OS"} />

      <Container open={open}>
        <Header>
          <InputWithLabelAtTheTop
            label="Pesquisar"
            placeholder="Cliente, número ou vendedor"
            ref={inputSearchRef}
            onChange={handleChangeSearchInputValue}
          />
          <Button
            color="#1AAE9F"
            onClick={() => {
              setOpenServiceOrderRegistrationModal(true);
            }}
          >
            <PlusIcon /> Nova OS
          </Button>
        </Header>
        <Section>
          <Title>Em execução:</Title>
          {filteredSO.SORunning.length !== 0 ? (
            <MyTableExcel
              style={{ margin: 0 }}
              columns={[
                { name: "Número da OS" },
                { name: "Cliente" },
                { name: "Vendedor" },
                { name: "Técnico" },
                { name: "Custo da OS" },
                { name: "Ações", align: "right" },
              ]}
            >
              {filteredSO.SORunning.map((so: SOProps) => (
                <TableRow key={so.id}>
                  <TableCell width={150} component="th" scope="row">
                    {so.SONumber}
                  </TableCell>
                  <TableCell align="left">{so.client}</TableCell>
                  <TableCell align="left">{so.seller}</TableCell>
                  <TableCell align="left">{so.responsibleTechnician}</TableCell>
                  <TableCell align="left">R$ {so.costOfSO}</TableCell>
                  <TableCell align="right" width={160}>
                    <ButtonWithIcon
                      tooltipText={"Fechar OS"}
                      Icon={IoKey}
                      onClick={() => {
                        closeOS(so.id);
                      }}
                    />
                    <ButtonWithIcon
                      tooltipText={"Adicionar Material à OS"}
                      Icon={ImBoxAdd}
                      onClick={() => {
                        openAddAMaterialFromAnOs(so);
                      }}
                    />
                    <ButtonWithIcon
                      tooltipText={"Retornar material ao estoque"}
                      Icon={ImBoxRemove}
                      onClick={() => {
                        openReturnAMaterialFromAnOs(so);
                      }}
                    />
                  </TableCell>
                </TableRow>
              ))}
            </MyTableExcel>
          ) : (
            <p style={{ marginTop: 20, fontWeight: 500 }}>
              Sem obras em execução
            </p>
          )}
        </Section>
        <Section>
          <Title>Obras concluidas:</Title>
          {filteredSO.SOCompleted.length !== 0 ? (
            <MyTableExcel
              style={{ margin: 0 }}
              columns={[
                { name: "Número da OS" },
                { name: "Cliente" },
                { name: "Vendedor" },
                { name: "Técnico" },
                { name: "Custo da OS" },
                { name: "Ações", align: "right" },
              ]}
            >
              {filteredSO.SOCompleted.map((so: SOProps) => (
                <TableRow key={so.id}>
                  <TableCell width={150} component="th" scope="row">
                    {so.SONumber}
                  </TableCell>
                  <TableCell align="left">{so.client}</TableCell>
                  <TableCell align="left">{so.seller}</TableCell>
                  <TableCell align="left">{so.responsibleTechnician}</TableCell>
                  <TableCell align="left">R$ {so.costOfSO}</TableCell>
                  <TableCell align="right" width={160}>
                    <ButtonWithIcon
                      tooltipText={"Abrir OS"}
                      Icon={FaLockOpen}
                      onClick={() => {
                        openOS(so.id);
                      }}
                    />
                    <ButtonWithIcon
                      tooltipText={"Fechar OS"}
                      Icon={FaLock}
                      onClick={() => {
                        openEndAnOs(so);
                      }}
                    />
                  </TableCell>
                </TableRow>
              ))}
            </MyTableExcel>
          ) : (
            <p style={{ marginTop: 20, fontWeight: 500 }}>
              Sem obras concluidas
            </p>
          )}
        </Section>
        <Section>
          <Title>Os Encerradas:</Title>
          {filteredSO.SOClosed.length !== 0 ? (
            <MyTableExcel
              style={{ margin: 0 }}
              columns={[
                { name: "Número da OS" },
                { name: "Cliente" },
                { name: "Vendedor" },
                { name: "Técnico" },
                { name: "Custo da OS" },
                { name: "Ações", align: "right" },
              ]}
            >
              {filteredSO.SOClosed.map((so: SOProps) => (
                <TableRow key={so.id}>
                  <TableCell width={150} component="th" scope="row">
                    {so.SONumber}
                  </TableCell>
                  <TableCell align="left">{so.client}</TableCell>
                  <TableCell align="left">{so.seller}</TableCell>
                  <TableCell align="left">{so.responsibleTechnician}</TableCell>
                  <TableCell align="left">R$ {so.costOfSO}</TableCell>
                  <TableCell align="right" width={160}>
                    <ButtonWithIcon
                      tooltipText={"Ver detalhes da OS"}
                      Icon={MdViewHeadline}
                      onClick={() => {
                        openOsDetails(so);
                      }}
                    />
                  </TableCell>
                </TableRow>
              ))}
            </MyTableExcel>
          ) : (
            <p style={{ marginTop: 20, fontWeight: 500 }}>
              Sem obras encerradas
            </p>
          )}
        </Section>
        <CreateSoModal
          openServiceOrderRegistrationModal={openServiceOrderRegistrationModal}
          setOpenServiceOrderRegistrationModal={
            setOpenServiceOrderRegistrationModal
          }
          openEditModal={openEditModal}
          setOpenEditModal={setOpenEditModal}
        />
        <AddMaterialModal
          prefilledServiceOrderId={serviceOrderId}
          openAddMaterialModal={openAddMaterialToAnSoModal}
          setOpenAddMaterialModal={setOpenAddMaterialToAnSoModal}
          isEditableModal={openEditModal}
          setIsEditableModal={setOpenEditModal}
          SONumber={SONumber}
          setSONumber={setSONumber}
          client={client}
          setClient={setClient}
          onSubmitPost
        />
        <ReturnMaterialModal
          serviceOrderId={serviceOrderId}
          openReturnAMaterialFromAnOsModal={openReturnAMaterialFromAnOsModal}
          setOpenReturnAMaterialFromAnOsModal={
            setOpenReturnAMaterialFromAnOsModal
          }
          SONumber={SONumber}
          setSONumber={setSONumber}
          client={client}
          setClient={setClient}
          materialsInSO={materialsInSO}
          setMaterialsInSO={setMaterialsInSO}
        />
        {openEndAnOsModal && (
          <Modal
            title="Encerrar uma OS"
            handleSubmit={handleEndAnOSSubmit}
            setOpenModal={setOpenEndAnOsModal}
          >
            <div>
              <Input label="Número da OS" disabled={true} value={SONumber} />
              <Input label="Cliente" disabled={true} value={client?.name} />
              <Input
                label="Custo da mão de obra"
                placeholder="Informe o custo da mão de obra"
                type="text"
                value={manPower === 0 ? "" : manPower}
                onChange={(e) => {
                  setManPower(Number(e.target.value));
                }}
              />
              <Input
                label="Custo do deslocamento"
                placeholder="Informe o custo de deslocamento"
                type="text"
                value={displacement === 0 ? "" : displacement}
                onChange={(e) => {
                  setDisplacement(Number(e.target.value));
                }}
              />
            </div>
            <>
              <Button
                type="button"
                outline
                color="#6558F5"
                onClick={() => {
                  setOpenEndAnOsModal(false);
                }}
              >
                Cancelar
              </Button>
              <div style={{ display: "flex" }}>
                <Button
                  outline
                  type="submit"
                  color="#1AAE9F"
                  onClick={() => setCloseAndSeeDetails(true)}
                >
                  Encerrar e ver Detalhes
                </Button>
                <Button
                  style={{ marginLeft: 20 }}
                  type="submit"
                  color="#1AAE9F"
                >
                  Encerrar OS
                </Button>
              </div>
            </>
          </Modal>
        )}
        {openOsDetailsModal && (
          <ModalServiceOrderDetail
            title="Detalhes da OS"
            setOpenModal={setOpenOsDetailsModal}
          >
            <div>
              <InputLine>
                <InputWithLabelAtTheTop
                  label="Número da OS"
                  disabled={true}
                  value={SONumber}
                />
                <InputWithLabelAtTheTop
                  label="Cliente"
                  disabled={true}
                  value={client?.name}
                />
              </InputLine>
              <InputLine>
                <InputWithLabelAtTheTop
                  label="Custo total dos Materiais"
                  disabled={true}
                  value={`R$ ${materialsTotalCost}`}
                />
                <InputWithLabelAtTheTop
                  label="Custo total da OS"
                  disabled={true}
                  value={`R$ ${totalCost}`}
                />
              </InputLine>
              <InputLine>
                <InputWithLabelAtTheTop
                  label="Custo da mão de obra"
                  disabled={true}
                  value={`R$ ${manPower}`}
                />
                <InputWithLabelAtTheTop
                  label="Custo do Deslocamento"
                  disabled={true}
                  value={`R$ ${displacement}`}
                />
              </InputLine>
              <InputLine>
                <InputWithLabelAtTheTop
                  label="Técnico"
                  disabled={true}
                  value={responsibleTechnician?.name}
                />
                <InputWithLabelAtTheTop
                  label="Vendedor"
                  disabled={true}
                  value={seller?.name}
                />
              </InputLine>
              <InputLine>
                <InputWithLabelAtTheTop
                  label="Data da abertura:"
                  disabled={true}
                  value={formatDate(openingDate)}
                />
                <InputWithLabelAtTheTop
                  label="Data do fechamento:"
                  disabled={true}
                  value={formatDate(closingDate)}
                />
              </InputLine>
              <ItemsPurchasedContainer>
                <Divisor />
                <TitleProducts>Materiais Utilizados</TitleProducts>
                {products.length ? (
                  <MyTableExcel
                    columns={[
                      { name: "Nome do Produto" },
                      { name: "Valor Unitário" },
                      { name: "Quantidade" },
                      { name: "Valor Total" },
                    ]}
                  >
                    {products.map((item) => (
                      <TableRow key={item.id}>
                        <TableCell component="th" scope="row">
                          {item.name}
                        </TableCell>
                        <TableCell align="left">R$ {item.value}</TableCell>
                        <TableCell align="left">{item.quantity}</TableCell>
                        <TableCell align="left">
                          R$ {item.quantity * item.value}
                        </TableCell>
                      </TableRow>
                    ))}
                  </MyTableExcel>
                ) : (
                  <Text>Nenhum material foi alocado a essa OS</Text>
                )}
              </ItemsPurchasedContainer>
            </div>
            <>
              <Button
                type="button"
                outline
                color="#6558F5"
                onClick={() => {
                  setOpenOsDetailsModal(false);
                }}
              >
                Fechar
              </Button>
              <div style={{ display: "flex" }}>
                <a
                  href={`/imprimir/${serviceOrderId}`}
                  rel="noreferrer"
                  target="_blank"
                  style={{ textDecoration: "none" }}
                >
                  <Button
                    type="button"
                    style={{ marginLeft: 20 }}
                    color="#1AAE9F"
                  >
                    Imprimir OS
                  </Button>
                </a>
              </div>
            </>
          </ModalServiceOrderDetail>
        )}
      </Container>
    </>
  );
};

export default SoManagement;
