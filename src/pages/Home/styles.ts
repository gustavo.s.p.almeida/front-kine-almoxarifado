import styled from "styled-components";

interface SidebarOpen {
  open: boolean;
}

interface WrapperProps extends SidebarOpen {}

export const Wrapper = styled.div<WrapperProps>`
  background-color: #dfe6ed;
  min-height: 100vh;

  transition: padding-left 0.5s;

  @media (min-width: 550px) {
    padding-left: ${({ open }) => (open ? "300px" : "0px")};
  }
`;

export const Container = styled.div`
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  gap: 40px;
  max-width: 875px;
  margin: 0 auto;
  padding: 120px 20px;
`;
