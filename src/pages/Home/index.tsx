import React, { useContext, useEffect, useState } from "react";
import Card from "../../components/Card";
import Header from "../../components/Header";
import { Wrapper, Container } from "./styles";
import { SideBarContext } from "../../contexts/SideBarContext";

import { useHistory } from "react-router-dom";

import {
  MdPlaylistAdd,
  MdAddShoppingCart,
  MdRemoveShoppingCart,
} from "react-icons/md";

import { FiShoppingBag } from "react-icons/fi";
import { IoMdReturnLeft } from "react-icons/io";
import { RiFileEditFill } from "react-icons/ri";
import AddMaterialModal from "../../components/AddMaterialModal";
import CreateSoModal from "../../components/CreateSoModal";
import ReturnMaterialModal from "../../components/ReturnMaterialModal";
import { InputSearchProps } from "../../components/InputSearch";

const Home: React.FC = () => {
  const { open } = useContext(SideBarContext);

  const [openEditModal, setOpenEditModal] = useState(false);

  const history = useHistory();
  const { setActivePage } = useContext(SideBarContext);

  const [modalAddEditableMaterial, setModalAddEditableMaterial] =
    useState(false);

  const [
    openServiceOrderRegistrationModal,
    setOpenServiceOrderRegistrationModal,
  ] = useState(false);

  const [openAddMaterialToAnSoModal, setOpenAddMaterialToAnSoModal] =
    useState(false);
  const [
    openReturnAMaterialFromAnOsModal,
    setOpenReturnAMaterialFromAnOsModal,
  ] = useState(false);

  const [client, setClient] = useState<InputSearchProps | null>(null);

  const [SONumber, setSONumber] = useState<string>("");

  const [materialsInSO, setMaterialsInSO] = useState<InputSearchProps[]>([]);

  function openNewPurchase() {
    setActivePage(2);
    history.push("estoque-atual");
  }
  function openCurrentStock() {
    setActivePage(1);
    history.push("estoque-atual");
  }
  function openSOManagement() {
    setActivePage(3);
    history.push("gestao-de-os");
  }

  function openAddMaterial() {
    setModalAddEditableMaterial(true);
    setOpenAddMaterialToAnSoModal(true);
  }

  async function openReturnAMaterialFromAnOs() {
    setOpenReturnAMaterialFromAnOsModal(true);
  }

  useEffect(() => {
    if (!openAddMaterialToAnSoModal) {
      setModalAddEditableMaterial(false);
    }
  }, [openAddMaterialToAnSoModal]);

  return (
    <>
      <Header pageName={"Início"} />
      <Wrapper open={open}>
        <CreateSoModal
          openServiceOrderRegistrationModal={openServiceOrderRegistrationModal}
          setOpenServiceOrderRegistrationModal={
            setOpenServiceOrderRegistrationModal
          }
          openEditModal={openEditModal}
          setOpenEditModal={setOpenEditModal}
        />
        <AddMaterialModal
          isEditableModal={modalAddEditableMaterial}
          setIsEditableModal={setModalAddEditableMaterial}
          openAddMaterialModal={openAddMaterialToAnSoModal}
          setOpenAddMaterialModal={setOpenAddMaterialToAnSoModal}
          SONumber={SONumber}
          setSONumber={setSONumber}
          client={client}
          setClient={setClient}
          onSubmitPost
        />

        <ReturnMaterialModal
          openReturnAMaterialFromAnOsModal={openReturnAMaterialFromAnOsModal}
          setOpenReturnAMaterialFromAnOsModal={
            setOpenReturnAMaterialFromAnOsModal
          }
          SONumber={SONumber}
          setSONumber={setSONumber}
          client={client}
          setClient={setClient}
          materialsInSO={materialsInSO}
          setMaterialsInSO={setMaterialsInSO}
          modalReturnMaterialEditable
        />
        <Container>
          <Card
            title="Estoque"
            text="Visualizar o estoque completo"
            Icon={FiShoppingBag}
            onClick={openCurrentStock}
          />
          <Card
            title="Nova Compra"
            text="Adicionar produtos ao estoque"
            Icon={MdAddShoppingCart}
            onClick={openNewPurchase}
          />
          <Card
            title="Saida do Estoque"
            text="Registrar a saída de itens do almoxarifado, para uma obra (Ordem de Serviço)"
            Icon={MdRemoveShoppingCart}
            onClick={openAddMaterial}
          />
          <Card
            title="Retorno ao Estoque"
            text="Registrar o retorno dos itens que tinham ido para uma obra"
            Icon={IoMdReturnLeft}
            onClick={openReturnAMaterialFromAnOs}
          />
          <Card
            title="Gerenciar OS's"
            text="Visualizar todas as OS do sistema"
            Icon={RiFileEditFill}
            onClick={openSOManagement}
          />
          <Card
            title="Nova OS"
            text="Registrar uma nova ordem de serviço"
            Icon={MdPlaylistAdd}
            onClick={() => {
              setOpenServiceOrderRegistrationModal(true);
            }}
          />
        </Container>
      </Wrapper>
    </>
  );
};

export default Home;
