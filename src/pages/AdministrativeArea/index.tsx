import React, { FormEvent, useContext, useEffect, useState } from "react";
import Header from "../../components/Header";
import MyTableExcel from "../../components/ExcelTable";
import Modal from "../../components/Modal";
import Input from "../../components/Input";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import Button from "../../components/Button";
import ButtonWithIcon from "../../components/ButtonWithIcon";
import {
  MdEdit,
  MdDeleteSweep,
  MdCheck,
  MdClose,
  MdDelete,
} from "react-icons/md";
import { SideBarContext } from "../../contexts/SideBarContext";
import api from "../../services/api";

import { Container, Section, Title } from "./styles";

import { InputSearchProps } from "../../components/InputSearch";

interface IUser {
  id: string;
  name: string;
  email: string;
  allow_access: boolean;
  status: "APROVADO" | "toEvaluate" | "REPROVADO" | "DELETADO";
}
interface Seller {
  id: string;
  name: string;
  created_at?: string;
  updated_at?: string;
}

interface Supplier {
  id: string;
  name: string;
  created_at?: string;
  updated_at?: string;
}

interface Client {
  id: string;
  name: string;
  created_at?: string;
  updated_at?: string;
}

interface Technician {
  id: string;
  name: string;
  created_at?: string;
  updated_at?: string;
}
interface Product {
  id: string;
  name: string;
  description: string;
  unit_cost: number;
  qty_stocked: number;
  max_stock_limit: number;
}
interface ErrorProps {
  response: {
    data: {
      message: string;
    };
  };
}

const AdministrativeArea: React.FC = () => {
  const { open } = useContext(SideBarContext);

  const [usersAwaitingApproval, setUsersAwaitingApproval] = useState<IUser[]>(
    []
  );
  const [registeredUsers, setRegisteredUsers] = useState<IUser[]>([]);

  const [seller, setSeller] = useState<Seller | null>(null);
  const [sellers, setSellers] = useState<Seller[]>([]);
  const [showEditSellerModal, setShowEditSellerModal] = useState(false);
  const [editedSeller, setEditedSeller] = useState<InputSearchProps | null>(
    null
  );

  const [supplier, setSupplier] = useState<Supplier | null>(null);
  const [suppliers, setSuppliers] = useState<Supplier[]>([]);
  const [showEditSupplierModal, setShowEditSupplierModal] = useState(false);
  const [editedSupplier, setEditedSupplier] = useState<InputSearchProps | null>(
    null
  );

  const [client, setClient] = useState<Client | null>(null);
  const [clients, setClients] = useState<Client[]>([]);
  const [showEditClientModal, setShowEditClientModal] = useState(false);
  const [editedClient, setEditedClient] = useState<InputSearchProps | null>(
    null
  );

  const [technician, setTechnician] = useState<Technician | null>(null);
  const [technicians, setTechnicians] = useState<Technician[]>([]);
  const [showEditTechnicianModal, setShowEditTechnicianModal] = useState(false);
  const [editedTechnician, setEditedTechnician] =
    useState<InputSearchProps | null>(null);

  const [product, setProduct] = useState<Product | null>(null);
  const [products, setProducts] = useState<Product[]>([]);
  const [showEditProductModal, setShowEditProductModal] = useState(false);
  const [editedProductName, setEditedProductName] = useState("");
  const [editedProductDescription, setEditedProductDescription] = useState("");
  const [editedProductUnitCost, setEditedProductUnitCost] = useState(0);
  const [editedProductQuantityInStock, setEditedProductQuantityInStock] =
    useState(0);
  const [editedProductMaxStockLimit, setEditedProductMaxStockLimit] =
    useState(0);

  async function getUsersToEvaluate() {
    try {
      const { data: usersToEvaluate } = await api.get(
        "/admins/usersToEvaluate"
      );
      setUsersAwaitingApproval(usersToEvaluate);
    } catch (error) {
      alert("Erro ao buscar usuários aguardando aprovação");
      console.log(error);
    }
  }
  async function getApprovedUsers() {
    try {
      const { data: approvedUsers } = await api.get("/admins/approvedUsers");
      setRegisteredUsers(approvedUsers);
    } catch (error) {
      alert("Erro ao buscar usuários aprovados");
      console.log(error);
    }
  }
  async function approveUser(id: string) {
    try {
      await api.put(`/admins/approveUser/${id}`);
      getUsersToEvaluate();
      getApprovedUsers();
    } catch (error) {
      const parsedError = error as ErrorProps;
      alert(parsedError?.response?.data?.message);
      console.log(error);
    }
  }
  async function disapproveUser(id: string) {
    try {
      await api.put(`/admins/reproveUser/${id}`);
      getUsersToEvaluate();
      getApprovedUsers();
    } catch (error) {
      const parsedError = error as ErrorProps;
      alert(parsedError?.response?.data?.message);
      console.log(error);
    }
  }
  async function deleteUser(id: string) {
    try {
      await api.delete(`/admins/deleteUser/${id}`);
      getApprovedUsers();
    } catch (error) {
      const parsedError = error as ErrorProps;
      alert(parsedError?.response?.data?.message);
      console.log(error);
    }
  }

  async function getSellers() {
    try {
      const { data } = await api.get("/sellers");
      setSellers(data);
    } catch (error) {
      alert("Erro ao buscar vendedores");
    }
  }
  async function removeSeller(seller: Seller) {
    try {
      await api.delete(`/sellers/${seller.id}`);
      alert(`O vendedor ${seller.name} foi removido`);
      getSellers();
    } catch (error) {
      alert("Falha ao deletar vendedor");
      console.log(error);
    }
  }
  function openEditSellerModal(seller: Seller) {
    setSeller(seller);
    setEditedSeller(seller);
    setShowEditSellerModal(true);
  }
  async function handleEditSellerModalSubmit(e: FormEvent) {
    e.preventDefault();
    try {
      const response = await api.patch("/sellers", {
        id: seller?.id,
        newName: editedSeller?.name,
      });
      setShowEditSellerModal(false);
      alert(`Nome do vendedor atualizado para: ${response?.data?.name}`);
      getSellers();
    } catch (error) {
      alert("Erro ao editar vendedor");
      console.log(error);
    }
  }

  async function getSuppliers() {
    try {
      const { data } = await api.get("/suppliers");
      setSuppliers(data);
    } catch (error) {
      alert("Erro ao buscar fornecedores");
    }
  }
  async function removeSupplier(supplier: Supplier) {
    try {
      await api.delete(`/suppliers/${supplier.id}`);
      alert(`O fornecedor ${supplier.name} foi removido`);
      getSuppliers();
    } catch (error) {
      alert("Falha ao deletar fornecedor");
      console.log(error);
    }
  }
  function openEditSupplierModal(supplier: Supplier) {
    setSupplier(supplier);
    setEditedSupplier(supplier);
    setShowEditSupplierModal(true);
  }
  async function handleEditSupplierModalSubmit(e: FormEvent) {
    e.preventDefault();
    try {
      const response = await api.patch("/suppliers", {
        id: supplier?.id,
        newName: editedSupplier?.name,
      });
      setShowEditSupplierModal(false);
      alert(`Nome do fornecedor atualizado para: ${response?.data?.name}`);
      getSuppliers();
    } catch (error) {
      alert("Erro ao editar fornecedor");
      console.log(error);
    }
  }

  async function getClients() {
    try {
      const { data } = await api.get("/clients");
      setClients(data);
    } catch (error) {
      alert("Erro ao buscar clientes");
    }
  }
  async function removeClient(client: Client) {
    try {
      await api.delete(`/clients/${client.id}`);
      alert(`O cliente ${client.name} foi removido`);
      getClients();
    } catch (error) {
      alert("Falha ao deletar cliente");
      console.log(error);
    }
  }
  function openEditClientModal(client: Client) {
    setClient(client);
    setEditedClient(client);
    setShowEditClientModal(true);
  }
  async function handleEditClientModalSubmit(e: FormEvent) {
    e.preventDefault();
    try {
      const response = await api.patch("/clients", {
        id: client?.id,
        newName: editedClient?.name,
      });
      setShowEditClientModal(false);
      alert(`Nome do cliente atualizado para: ${response?.data?.name}`);
      getClients();
    } catch (error) {
      alert("Erro ao editar cliente");
      console.log(error);
    }
  }

  async function getTechnicians() {
    try {
      const { data } = await api.get("/technicians");

      setTechnicians(data);
    } catch (error) {
      alert("Erro ao buscar técnicos");
    }
  }
  async function removeTechnician(technician: Technician) {
    try {
      await api.delete(`/technicians/${technician.id}`);
      alert(`O técnico ${technician.name} foi removido`);
      getTechnicians();
    } catch (error) {
      alert("Falha ao deletar técnico");
      console.log(error);
    }
  }
  function openEditTechnicianModal(technician: Technician) {
    setTechnician(technician);
    setEditedTechnician(technician);
    setShowEditTechnicianModal(true);
  }
  async function handleEditTechnicianModalSubmit(e: FormEvent) {
    e.preventDefault();
    try {
      const response = await api.patch("/technicians", {
        id: technician?.id,
        newName: editedTechnician?.name,
      });
      setShowEditTechnicianModal(false);
      alert(`Nome do técnico atualizado para: ${response?.data?.name}`);
      getTechnicians();
    } catch (error) {
      alert("Erro ao editar técnico");
      console.log(error);
    }
  }

  async function getProducts() {
    try {
      const { data } = await api.get("/products");
      setProducts(data);
    } catch (error) {
      alert("Erro ao buscar produtos");
    }
  }
  async function removeProduct(product: Product) {
    try {
      await api.delete(`/products/${product.id}`);
      alert(`O produto ${product.name} foi removido`);
      getProducts();
    } catch (error) {
      alert("Falha ao deletar produto");
      console.log(error);
    }
  }
  function openEditProductModal(product: Product) {
    setProduct(product);
    setEditedProductName(product.name.toString());
    setEditedProductDescription(product.description.toString());
    setEditedProductUnitCost(product.unit_cost);
    setEditedProductQuantityInStock(product.qty_stocked);
    setEditedProductMaxStockLimit(product.max_stock_limit);
    setShowEditProductModal(true);
  }
  async function handleEditProductModalSubmit(e: FormEvent) {
    e.preventDefault();
    try {
      await api.patch("/products", {
        id: product?.id,
        newName: editedProductName,
        newDescription: editedProductDescription,
        newUnitCost: editedProductUnitCost,
        newQuantityInStock: editedProductQuantityInStock,
        newMaxStockLimit: editedProductMaxStockLimit,
      });
      setShowEditProductModal(false);
      alert(`Dados do produto atualizados com sucesso`);
      getProducts();
    } catch (error) {
      alert("Erro ao editar produto");
      console.log(error);
    }
  }

  useEffect(() => {
    getUsersToEvaluate();
    getApprovedUsers();
    getSellers();
    getSuppliers();
    getClients();
    getTechnicians();
    getProducts();
  }, []);

  return (
    <>
      <Header pageName="Gerenciar Base de Dados" />

      <Container open={open}>
        <Section>
          <Title>Usuários aguardando aprovação:</Title>
          {usersAwaitingApproval.length !== 0 ? (
            <MyTableExcel
              style={{ margin: 0 }}
              columns={[
                { name: "Nome" },
                { name: "Email" },
                { name: "Status" },
                { name: "Ações", align: "right" },
              ]}
            >
              {usersAwaitingApproval.map((user: IUser) => (
                <TableRow key={user.id}>
                  <TableCell width={150} component="td" scope="row">
                    {user.name}
                  </TableCell>
                  <TableCell width={150} component="td" scope="row">
                    {user.email}
                  </TableCell>
                  <TableCell width={150} component="td" scope="row">
                    {user.status === "toEvaluate" ? "Aguardando aprovação" : ""}
                  </TableCell>
                  <TableCell align="right" width={1}>
                    <ButtonWithIcon
                      tooltipText={"Aprovar usuário"}
                      Icon={MdCheck}
                      onClick={() => {
                        approveUser(user.id);
                      }}
                    />
                    <ButtonWithIcon
                      tooltipText={"Reprovar usuário"}
                      Icon={MdClose}
                      onClick={() => {
                        disapproveUser(user.id);
                      }}
                    />
                  </TableCell>
                </TableRow>
              ))}
            </MyTableExcel>
          ) : (
            <p style={{ marginTop: 20, fontWeight: 500 }}>
              Nenhum usuário aguardando aprovação
            </p>
          )}
        </Section>

        <Section>
          <Title>Usuários Aprovados:</Title>
          {registeredUsers.length !== 0 ? (
            <MyTableExcel
              style={{ margin: 0 }}
              columns={[
                { name: "Nome" },
                { name: "Email" },
                { name: "Status" },
                { name: "Ações", align: "right" },
              ]}
            >
              {registeredUsers.map((user: IUser) => (
                <TableRow key={user.id}>
                  <TableCell width={150} component="td" scope="row">
                    {user.name}
                  </TableCell>
                  <TableCell width={150} component="td" scope="row">
                    {user.email}
                  </TableCell>
                  <TableCell width={150} component="td" scope="row">
                    {user.status === "APROVADO" ? "Aprovado" : ""}
                  </TableCell>
                  <TableCell align="right" width={1}>
                    <ButtonWithIcon
                      tooltipText={"Reprovar usuário"}
                      Icon={MdDelete}
                      onClick={() => {
                        deleteUser(user.id);
                      }}
                    />
                  </TableCell>
                </TableRow>
              ))}
            </MyTableExcel>
          ) : (
            <p style={{ marginTop: 20, fontWeight: 500 }}>
              Nenhum usuário autorizado no sistema
            </p>
          )}
        </Section>

        <Section>
          <Title>Vendedores:</Title>
          {sellers.length !== 0 ? (
            <MyTableExcel
              style={{ margin: 0 }}
              columns={[{ name: "Nome" }, { name: "Ações", align: "right" }]}
            >
              {sellers.map((seller: Seller) => (
                <TableRow key={seller.id}>
                  <TableCell width={150} component="td" scope="row">
                    {seller.name}
                  </TableCell>
                  <TableCell align="right" width={1}>
                    <ButtonWithIcon
                      tooltipText={"Editar vendedor"}
                      Icon={MdEdit}
                      onClick={() => {
                        openEditSellerModal(seller);
                      }}
                    />
                    <ButtonWithIcon
                      tooltipText={"Deletar vendedor"}
                      Icon={MdDeleteSweep}
                      onClick={() => {
                        removeSeller(seller);
                      }}
                    />
                  </TableCell>
                </TableRow>
              ))}
            </MyTableExcel>
          ) : (
            <p style={{ marginTop: 20, fontWeight: 500 }}>
              Nenhum vendedor encontrado
            </p>
          )}
        </Section>

        <Section>
          <Title>Fornecedores:</Title>
          {suppliers.length !== 0 ? (
            <MyTableExcel
              style={{ margin: 0 }}
              columns={[{ name: "Nome" }, { name: "Ações", align: "right" }]}
            >
              {suppliers.map((supplier: Supplier) => (
                <TableRow key={supplier.id}>
                  <TableCell width={150} component="td" scope="row">
                    {supplier.name}
                  </TableCell>
                  <TableCell align="right" width={1}>
                    <ButtonWithIcon
                      tooltipText={"Editar fornecedor"}
                      Icon={MdEdit}
                      onClick={() => {
                        openEditSupplierModal(supplier);
                      }}
                    />
                    <ButtonWithIcon
                      tooltipText={"Deletar fornecedor"}
                      Icon={MdDeleteSweep}
                      onClick={() => {
                        removeSupplier(supplier);
                      }}
                    />
                  </TableCell>
                </TableRow>
              ))}
            </MyTableExcel>
          ) : (
            <p style={{ marginTop: 20, fontWeight: 500 }}>
              Nenhum fornecedor encontrado
            </p>
          )}
        </Section>

        <Section>
          <Title>Clientes:</Title>
          {clients.length !== 0 ? (
            <MyTableExcel
              style={{ margin: 0 }}
              columns={[{ name: "Nome" }, { name: "Ações", align: "right" }]}
            >
              {clients.map((client: Client) => (
                <TableRow key={client.id}>
                  <TableCell width={150} component="td" scope="row">
                    {client.name}
                  </TableCell>
                  <TableCell align="right" width={1}>
                    <ButtonWithIcon
                      tooltipText={"Editar cliente"}
                      Icon={MdEdit}
                      onClick={() => {
                        openEditClientModal(client);
                      }}
                    />
                    <ButtonWithIcon
                      tooltipText={"Deletar cliente"}
                      Icon={MdDeleteSweep}
                      onClick={() => {
                        removeClient(client);
                      }}
                    />
                  </TableCell>
                </TableRow>
              ))}
            </MyTableExcel>
          ) : (
            <p style={{ marginTop: 20, fontWeight: 500 }}>
              Nenhum cliente encontrado
            </p>
          )}
        </Section>

        <Section>
          <Title>Técnicos:</Title>
          {technicians.length !== 0 ? (
            <MyTableExcel
              style={{ margin: 0 }}
              columns={[{ name: "Nome" }, { name: "Ações", align: "right" }]}
            >
              {technicians.map((technician: Technician) => (
                <TableRow key={technician.id}>
                  <TableCell width={150} component="td" scope="row">
                    {technician.name}
                  </TableCell>
                  <TableCell align="right" width={1}>
                    <ButtonWithIcon
                      tooltipText={"Editar técnico"}
                      Icon={MdEdit}
                      onClick={() => {
                        openEditTechnicianModal(technician);
                      }}
                    />
                    <ButtonWithIcon
                      tooltipText={"Deletar técnico"}
                      Icon={MdDeleteSweep}
                      onClick={() => {
                        removeTechnician(technician);
                      }}
                    />
                  </TableCell>
                </TableRow>
              ))}
            </MyTableExcel>
          ) : (
            <p style={{ marginTop: 20, fontWeight: 500 }}>
              Nenhum técnico encontrado
            </p>
          )}
        </Section>

        <Section>
          <Title>Produtos:</Title>
          {products.length !== 0 ? (
            <MyTableExcel
              style={{ margin: 0 }}
              columns={[{ name: "Nome" }, { name: "Ações", align: "right" }]}
            >
              {products.map((product: Product) => (
                <TableRow key={product.id}>
                  <TableCell width={150} component="td" scope="row">
                    {product.name}
                  </TableCell>
                  <TableCell align="right" width={1}>
                    <ButtonWithIcon
                      tooltipText={"Editar produto"}
                      Icon={MdEdit}
                      onClick={() => {
                        openEditProductModal(product);
                      }}
                    />
                    <ButtonWithIcon
                      tooltipText={"Deletar produto"}
                      Icon={MdDeleteSweep}
                      onClick={() => {
                        removeProduct(product);
                      }}
                    />
                  </TableCell>
                </TableRow>
              ))}
            </MyTableExcel>
          ) : (
            <p style={{ marginTop: 20, fontWeight: 500 }}>
              Nenhum produto encontrado
            </p>
          )}
        </Section>
      </Container>

      {showEditSellerModal && (
        <Modal
          title="Editar vendedor"
          handleSubmit={handleEditSellerModalSubmit}
          setOpenModal={setShowEditSellerModal}
        >
          <div>
            <Input
              inputSearch
              data={sellers}
              label="Nome do vendedor"
              placeholder="Insira o nome do vendedor"
              inputSearchValue={editedSeller}
              setValue={setEditedSeller}
            />
          </div>
          <>
            <Button
              type="button"
              outline
              color="#6558F5"
              onClick={() => {
                setShowEditSellerModal(false);
              }}
            >
              Cancelar
            </Button>
            <Button type="submit" style={{ width: 160 }} color="#1AAE9F">
              Salvar alterações
            </Button>
          </>
        </Modal>
      )}

      {showEditSupplierModal && (
        <Modal
          title="Editar fornecedor"
          handleSubmit={handleEditSupplierModalSubmit}
          setOpenModal={setShowEditSupplierModal}
        >
          <div>
            <Input
              inputSearch
              data={suppliers}
              label="Nome do fornecedor"
              placeholder="Insira o nome do fornecedor"
              inputSearchValue={editedSupplier}
              setValue={setEditedSupplier}
            />
          </div>
          <>
            <Button
              type="button"
              outline
              color="#6558F5"
              onClick={() => {
                setShowEditSupplierModal(false);
              }}
            >
              Cancelar
            </Button>
            <Button type="submit" style={{ width: 160 }} color="#1AAE9F">
              Salvar alterações
            </Button>
          </>
        </Modal>
      )}

      {showEditClientModal && (
        <Modal
          title="Editar cliente"
          handleSubmit={handleEditClientModalSubmit}
          setOpenModal={setShowEditClientModal}
        >
          <div>
            <Input
              inputSearch
              data={clients}
              label="Nome do cliente"
              placeholder="Insira o nome do cliente"
              inputSearchValue={editedClient}
              setValue={setEditedClient}
            />
          </div>
          <>
            <Button
              type="button"
              outline
              color="#6558F5"
              onClick={() => {
                setShowEditClientModal(false);
              }}
            >
              Cancelar
            </Button>
            <Button type="submit" style={{ width: 160 }} color="#1AAE9F">
              Salvar alterações
            </Button>
          </>
        </Modal>
      )}

      {showEditTechnicianModal && (
        <Modal
          title="Editar técnico"
          handleSubmit={handleEditTechnicianModalSubmit}
          setOpenModal={setShowEditTechnicianModal}
        >
          <div>
            <Input
              inputSearch
              data={technicians}
              label="Nome do técnico"
              placeholder="Insira o nome do técnico"
              inputSearchValue={editedTechnician}
              setValue={setEditedTechnician}
            />
          </div>
          <>
            <Button
              type="button"
              outline
              color="#6558F5"
              onClick={() => {
                setShowEditTechnicianModal(false);
              }}
            >
              Cancelar
            </Button>
            <Button type="submit" style={{ width: 160 }} color="#1AAE9F">
              Salvar alterações
            </Button>
          </>
        </Modal>
      )}

      {showEditProductModal && (
        <Modal
          title="Editar produto"
          handleSubmit={handleEditProductModalSubmit}
          setOpenModal={setShowEditProductModal}
        >
          <div>
            <Input
              label="Nome do produto"
              placeholder="Insira o nome do produto"
              type={"text"}
              value={editedProductName}
              onChange={(e) => {
                setEditedProductName(e.target.value.toString());
              }}
            />
            <Input
              label="Descrição"
              placeholder="Insira uma descrição para o produto"
              type={"text"}
              value={editedProductDescription}
              onChange={(e) => {
                setEditedProductDescription(e.target.value.toString());
              }}
            />
            <Input
              label="Custo unitário"
              placeholder="Insira o custo unitário"
              type="text"
              min="0"
              value={editedProductUnitCost}
              onChange={(e) => {
                setEditedProductUnitCost(Number(e.target.value));
              }}
            />
            <Input
              label="Estoque atual"
              placeholder="Insira a quantidade em estoque"
              type="text"
              min="0"
              value={editedProductQuantityInStock}
              onChange={(e) => {
                setEditedProductQuantityInStock(Number(e.target.value));
              }}
            />
            <Input
              label="Limite máximo do estoque"
              placeholder="Informe quanto deve ser o máximo em estoque para esse produto"
              type="text"
              min="0"
              value={editedProductMaxStockLimit}
              onChange={(e) => {
                setEditedProductMaxStockLimit(Number(e.target.value));
              }}
            />
          </div>
          <>
            <Button
              type="button"
              outline
              color="#6558F5"
              onClick={() => {
                setShowEditProductModal(false);
              }}
            >
              Cancelar
            </Button>
            <Button type="submit" style={{ width: 160 }} color="#1AAE9F">
              Salvar alterações
            </Button>
          </>
        </Modal>
      )}
    </>
  );
};

export default AdministrativeArea;
